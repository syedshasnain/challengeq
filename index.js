
'use strict'
const CHUNKS = 4
const CHUNK_SIZE = 1048576 // 1 MiB to bytes

// Load Modules
const argv = require('yargs-parser')(process.argv.slice(2))
const axios = require('axios')
const fs = require('fs')
const validUrl = require('valid-url')

let fileName
/**
 * Handle error function to show error message and exit application
 * @param {string} message The message of the error to be shown to user
 */
function handleError (message) {
  console.error('ERROR: ', message)
  process.exit()
}
/**
 * Pre-check validations of input from user
 */
function validateInput () {
  // Check if any arguments are passed
  if (argv._.length < 1) {
    handleError('The required argument for url is missing. Please define via --url=<linkhere> tag')
  }
  // Check if url argument is not null
  if (argv._[0].trim === '') {
    handleError('The required argument for url is missing. Please define via --url=<linkhere> tag')
  }
  // Validate if url is correct
  if (!validUrl.isUri(argv._[0])) {
    handleError('The provided url is not valid. Please try again with a valid URL')
  }
  // Check if Filename is provided
  if (typeof (argv._[1]) === 'undefined' || argv._[1].trim === '') {
    fileName = argv._[0].substring(argv._[0].lastIndexOf('/') + 1)
  } else {
    fileName = argv._[1]
  }
  // Check if duplicate File exists
  if (fs.existsSync(fileName)) {
    handleError(`Filename ${fileName} already exists.`)
  }
}

/**
 * Check file size and verifies headers
 * @param {string} url The url to Download
 */
async function checkFileSizeValidation (url) {
  try {
    const response = await axios({
      method: 'HEAD',
      url: url,
      responseType: 'stream',
      encoding: null
    })
    if (response.headers['content-length'] && response.headers['accept-ranges']) {
      if (parseInt(response.headers['content-length']) < CHUNK_SIZE * CHUNKS) {
        handleError('Cannot process file exceeding 4 Mib. It can easily be changed with CHUNKS and CHUNK_SIZE variable in code')
      }
    } else {
      handleError('Could not determine file size. Please input another url')
    }
  } catch (err) {
    handleError(`Something went wrong. ${err}`)
  }
}
/**
 * Downloads the file.
 *
 * @param {number} iterator Gets the number iterator required to download
 * @param {string} url The url to Download
 * @param {string} file The name of the file
 * @returns {Promise<boolean>} Returns true if file write has been completed
 */
async function download (iterator, url, file) {
  const writer = fs.createWriteStream(file, { flags: 'a' })
  let bytes
  if (iterator === 0) {
    bytes = `${CHUNK_SIZE * iterator}-${(CHUNK_SIZE * (iterator + 1))}`
  } else {
    if (iterator === CHUNKS - 1) {
      bytes = `${(CHUNK_SIZE * iterator) + 1}-${(CHUNK_SIZE * (iterator + 1) - 1)}`
    } else {
      bytes = `${(CHUNK_SIZE * iterator) + 1}-${CHUNK_SIZE * (iterator + 1)}`
    }
  }
  const response = await axios({
    method: 'get',
    url: url,
    responseType: 'stream',
    encoding: null,
    headers: {
      'Range': `bytes=${bytes}`,
      responseType: 'arraybuffer'
    }
  })
  response.data.pipe(writer)
  return new Promise((resolve, reject) => {
    writer.on('finish', resolve(true))
    writer.on('error', reject)
  })
}

/**
 * Main function to call the internal download function
 *
 * @param {string} url The url to Download
 * @param {string} file The name of the file
 * @returns {boolean} Returns true if all chunks are completed
 */
async function fetchAndWriteToFile (url, file) {
  try {
    for (let i = 0; i < CHUNKS; i++) {
      await download(i, url, file)
      console.log(`Chunk -${i} Downloaded`)
    }
    return true
  } catch (err) {
    handleError(`Something went wrong. ${err}`)
  }
}

/*
 * Main Block
 */

validateInput()
const url = argv._[0]

checkFileSizeValidation(url)

fetchAndWriteToFile(url, fileName).then(result => {
  if (result === true) {
    console.log(`The file ${fileName} is downloaded.`)
  }
}).catch((err) => {
  handleError(`Something went wrong. ${err}`)
})
